package org.sfans.admin.controller.api;

import static org.sfans.admin.utils.WebUtil.updatePageable;

import java.util.Date;

import javax.validation.Valid;

import org.sfans.admin.domain.WebsiteForm;
import org.sfans.admin.utils.ModelMapper;
import org.sfans.core.domain.Website;
import org.sfans.core.domain.WebsiteRepository;
import org.sfans.core.domain.Website.Status;
import org.sfans.core.web.exception.ResourceConflictException;
import org.sfans.core.web.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Profile("admin")
@RestController
@RequestMapping("${sfans.admin.urls.api}/websites")
@Secured("ROLE_ADMIN")
public class WebsiteController {

	private static final Logger LOG = LoggerFactory.getLogger(WebsiteController.class);

	@Autowired
	private WebsiteRepository repository;

	@Autowired
	private ModelMapper mapper;

	@Value("${sfans.admin.domain.website.items-per-page:20}")
	private int itemsPerPage;

	@RequestMapping(method = RequestMethod.GET)
	@Transactional(readOnly = true)
	public Page<Website> websites(final Pageable pageInfo) {
		final Pageable pageable = updatePageable(pageInfo, itemsPerPage);

		LOG.debug("websites listing: {}", pageable);

		return repository.findAll(pageable);
	}

	@RequestMapping(method = RequestMethod.GET, params = "status")
	@Transactional(readOnly = true)
	public Page<Website> websitesByStatus(@RequestParam("status") final Website.Status status,
			@PageableDefault(size = 10) final Pageable pageInfo) {
		final Pageable pageable = updatePageable(pageInfo, itemsPerPage);

		LOG.debug("Websites listing filtered by status:{}, {} ", status, pageable);

		return repository.findByStatus(status, pageable);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@Transactional(readOnly = true)
	public Website website(@PathVariable("id") final Long id) {
		LOG.debug("Requesting website:[{}] details", id);

		return findWebsite(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	@Transactional
	public Website update(@PathVariable("id") final Long id,
			@RequestBody @Valid final WebsiteForm form, final BindingResult result) {

		LOG.debug("Updating website:[{}] with content:{} (error:{})", id, form, result.hasErrors());

		final Website website = findWebsite(id);
		mapper.update(form, website);
		website.setUpdatedAt(new Date());

		return repository.save(website);
	}

	@RequestMapping(value = "/{id}/disable", method = RequestMethod.PATCH)
	@Transactional
	public Website disable(@PathVariable("id") final Long id) {
		LOG.debug("Disabling website:[{}]", id);

		final Website website = findWebsite(id);

		if (website.getStatus() != Status.ENABLED) {
			throw new ResourceConflictException("状态无效");
		}
		website.setUpdatedAt(new Date());
		website.setStatus(Status.DISABLED);

		repository.save(website);
		return website;
	}

	@RequestMapping(value = "/{id}/enable", method = RequestMethod.PATCH)
	@Transactional
	public Website enable(@PathVariable("id") final Long id) {
		LOG.debug("Enabling website:[{}]", id);

		final Website website = findWebsite(id);
		if (website.getStatus() != Status.DISABLED) {
			throw new ResourceConflictException("状态无效");
		}
		website.setUpdatedAt(new Date());
		website.setStatus(Status.ENABLED);

		repository.save(website);

		return website;
	}

	@RequestMapping(value = "/{id}/restore", method = RequestMethod.PATCH)
	@Transactional
	public Website restore(@PathVariable("id") final Long id) {
		LOG.debug("Restoring website:[{}]", id);

		final Website website = findWebsite(id);
		if (website.getStatus() != Status.PENDING_REMOVE) {
			throw new ResourceConflictException("状态无效");
		}

		website.setUpdatedAt(new Date());
		website.setStatus(Status.DISABLED);

		repository.save(website);
		return website;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@Transactional
	public void delete(@PathVariable("id") final Long id) {
		LOG.debug("Deleting website:[{}]", id);

		final Website website = findWebsite(id);

		website.setUpdatedAt(new Date());
		website.setDeleteRequestedAt(new Date());
		website.setStatus(Status.PENDING_REMOVE);

		repository.save(website);
	}

	private Website findWebsite(final Long id) {
		if (!repository.exists(id)) {
			throw new ResourceNotFoundException(String.format("Website ID:%s 不存在", id));
		}
		return repository.findOne(id);
	}

}
